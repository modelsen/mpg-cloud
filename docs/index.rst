RDMHelper documentation
=======================

This project collects small tools to help with the management of research data.

The development is part of the research project `ModelSEN <https://modelsen.mpiwg-berlin.mpg.de>`_
Socio-epistemic networks: Modelling Historical Knowledge Processes,
part of Department I of the Max Planck Institute for the History of Science and
funded by the Federal Ministry of Education and Research, Germany (Grant No. 01 UG2131).

.. image:: _static/bmbf.png

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme
   authors
   license




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
